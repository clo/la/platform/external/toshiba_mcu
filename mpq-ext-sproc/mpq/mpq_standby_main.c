/*
Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>

#include <mpq_standby_utils.h>

int mpq_standby_term(void)
{
   int ret = 0;

   /* Terminate CEC Module */

   /* Terminate Other Modules */

   /* Terminate Host Communication */
   ret = mpq_standby_hostcomm_term();

   /* Terminate IR Module */
   ret = mpq_standby_ir_rc_term();

   return ret;
}

int mpq_standby_init(void)
{
   int ret = 0;

   /* Initialize IR Module */
   ret = mpq_standby_ir_rc_init();
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in mpq_standby_ir_rc_init with %d\n", ret);
      goto cleanup;
   }

   /* Initialize CEC Module */

   /* Initialize Other Modules */

   /* Initialize Host Communication */
   MPQ_SP_DBG_MSG("Calling mpq_standby_hostcomm_init\n");
   ret = mpq_standby_hostcomm_init();
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in mpq_standby_hostcomm_init with %d\n", ret);
      goto cleanup;
   }

   return 0;
cleanup:
   mpq_standby_term();
   return ret;
}

int main()
{
   int ret  = 0;
   ret = mpq_standby_init();
   if (ret)
   {
      MPQ_SP_DBG_MSG("Failed in mpq_standby_init with %d\n", ret);
   }

   ret = mpq_standby_term();

   return 0;
}
