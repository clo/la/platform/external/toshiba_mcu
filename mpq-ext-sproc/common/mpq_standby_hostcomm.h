/*
Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef _MPQ_STANDBY_HOSTCOMM_H
#define _MPQ_STANDBY_HOSTCOMM_H

/*
 * Standby Processor Firmware Version.
 */
#define SP_FIRMWARE_VERSION 0x02

/*
 * Host Communication Protocol Indexes.
 */
#define HOSTCOMM_STARTCODE_INDEX 0x00
#define HOSTCOMM_SIZE_INDEX      0x01
#define HOSTCOMM_GROUP_INDEX     0x02
#define HOSTCOMM_COMMAND_INDEX   0x03
#define HOSTCOMM_DATA_INDEX      0x04

/* Start Code */
#define SP_COMM_STARTCODE 0xAA

/* Group ID */
#define SP_COMM_GRP_ACK       0x02
#define SP_GRP_IR_RC          0x03
#define SP_GRP_CTRL_MSG       0x04
#define SP_GRP_HDMI_CEC       0x05
#define SP_GRP_HOTPLUG_DETECT 0x06
#define SP_GRP_POWER_OFF      0x07

/* Commands */
/*
 * IR Commands
 */
#define SP_IR_SCANCODE_RCVD_EVENT 0x01

/*
 * CEC Commands
 */
#define SP_HDMI_CEC_MSG_EVENT 0x01

/*
 * Hot Plug Detect Commands.
 */
#define SP_HDMI_HOTPLUG_EVENT 0x01
#define SP_VGA_HOTPLUG_EVENT  0x02

/*
 * Control Commands
 */
typedef struct sp_caps
{
   unsigned char fw_version;
   unsigned char ir_enabled;
   unsigned char cec_enabled;
} MPQ_SP_PROC_CAPS;

#define SP_CTRL_MSG_SP_CAPS          0x01
#define SP_CTRL_MSG_MPQ_READY        0x02
#define SP_CTRL_MPQ_STATE_NW_STANDBY 0x03
#define SP_CTRL_MPQ_STATE_STANDBY    0x04


#endif /* _MPQ_STANDBY_HOSTCOMM_H */
