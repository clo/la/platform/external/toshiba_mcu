/*
 * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 *
 * Copyright (c) 2010 TOSHIBA CORPORATION, All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "sp_common.h"
#include "tmpm330_uart.h"
#include "sp_debugcomm.h"

#include <string.h>

extern unsigned char sleep;

unsigned char debug_tx_buffer[SIZE_OF_DEBUG_BUFFER];
unsigned char debug_tx_counter;
unsigned char debug_tx_size;

void INTTX1_IRQHandler(void)
{
   volatile UART_Err err;

   if (debug_tx_counter < debug_tx_size)
   {
      UART_SetTxData(UART1, debug_tx_buffer[debug_tx_counter++]);
   }
   else
   {
      err = UART_GetErrState(UART1);
      while(UART1->MOD2 & 0x20);
      sleep = TRUE;
   }
}

void INTRX1_IRQHandler(void)
{
   return;
}

void SP_DebugCommInit(void)
{
   UART_InitTypeDef myUART;

   SIO_Configuration(UART1);

   myUART.BaudRate = 115200U;
   myUART.DataBits = UART_DATA_BITS_8;
   myUART.StopBits = UART_STOP_BITS_1;
   myUART.Parity = UART_NO_PARITY;
   myUART.Mode = UART_ENABLE_RX | UART_ENABLE_TX;
   myUART.FlowCtrl = UART_NONE_FLOW_CTRL;

   UART_Enable(UART1);
   UART_Init(UART1, &myUART);
   UART_SetIdleMode(UART1, ENABLE);

   NVIC_Enable_Interrupt(INTTX1_interrupt);
   //NVIC_Enable_Interrupt(INTRX1_interrupt);
}

void SP_DebugUARTSendData(void)
{
   sleep = 0;

   /* Reset the Tx Counter */
   debug_tx_counter = 0;
   debug_tx_size    = strlen((char *)debug_tx_buffer);

   /* Send the Data through UART */
   UART_SetTxData(UART1, debug_tx_buffer[debug_tx_counter++]);
}
