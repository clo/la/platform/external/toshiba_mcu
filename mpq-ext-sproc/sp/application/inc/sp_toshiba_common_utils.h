/*
 * Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only.
 *
 * Copyright (c) 2010 TOSHIBA CORPORATION, All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _SP_TOSHIBA_COMMON_UTILS_H
#define _SP_TOSHIBA_COMMON_UTILS_H

#define SP_RedLEDPower(on)   GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_0, on)
#define SP_GreenLEDPower(on) GPIO_WriteDataBit(GPIO_PI, GPIO_BIT_1, on)

void SIO_Configuration(TSB_SC_TypeDef * SCx);
void SP_HardwareInit(void);
void SP_IssuePMICReset(void);
void SP_PowerOffPMIC(void);
void SP_HandleHDMIHotplug(void);
void SP_HandleVGAHotplug(void);
void SP_ResetMCU(void);
void SP_FRCPowerOn(void);
void SP_FRCPowerOff(void);
void SP_WaitForDelay(unsigned int delay_ms);
void SP_HandleMPQWakeup(void);
void SP_TimerCaptureInit(void);

#endif /* _SP_TOSHIBA_COMMON_UTILS_H */
