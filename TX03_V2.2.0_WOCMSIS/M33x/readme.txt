(C)Copyright TOSHIBA CORPORATION 2012 All rights reserved
                                                       @date: 2012/10/17

=====================================================
Device Peripheral Access Layer for TX03 package
=====================================================

This document provides the following information about Device 
Peripheral Access Layer for TX03 package:

  1) File organization after installation
  2) Technical information

========================================================================
  File organization after installation
------------------------------------------------------------------------

  \TMPM330
    \Libraries
      \TX03_M3            : TMPM330 Device Files
      \TX03_Periph_Driver : TMPM330 Peripheral Driver
    \Project
      \Examples           : TMPM330 Project Examples
      \Template           : TMPM330 Project Template
    \Utilities            : Sample programs used in examples

  \Documentation
    \M33x_Driver_UserGuide_E.pdf: User Guide (English)
    \M33x_Driver_UserGuide_J.pdf: User Guide (Japanese)
    \PPG_generation_Appnote.doc : Appnote for TMRB PPG mode operation

------------------------------------------------------------------------
* root directory
------------------------------------------------------------------------
readme.txt        This file
License.txt       Software license agreement

--[EOF]--
