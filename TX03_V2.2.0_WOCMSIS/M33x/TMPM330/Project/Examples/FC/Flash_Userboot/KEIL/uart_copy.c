/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* File Name          : uart_copy.c
* Version            : 
* Date               : 
* Description        : 
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/
#include "main.h"
#include "tmpm330_uart.h"
#include "uart_copy.h"

#define TERMINATION_STR "TERMINATE"
UART_InitTypeDef myUART;
uint8_t rec_data[512];
void SIO_Configuration(TSB_SC_TypeDef * SCx);


/**
 * \brief
 *    It compares two string, with string lengh pass to the function.
 *  If Two string equals than it returns zero otherwise 1.
 *
 * \return none
 */
#pragma arm section code="FLASH_ROM"
int compare_str(const char *s1, const char *s2,uint32_t strlenth)
{
    int i;

    for(i = 0;i < strlenth;i++)
    {
         if(s1[i] != s2[i])
	        return 1;
    }
    return 0;
}


#pragma arm section code="FLASH_ROM"
uint8_t uart_copy(void)
{
	int8_t ret_val = 0;
	uint32_t i, flash_add = 0;

	SIO_Configuration(UART0);
    myUART.BaudRate = 57600U;
    myUART.DataBits = UART_DATA_BITS_8;
    myUART.StopBits = UART_STOP_BITS_1;
    myUART.Parity = UART_NO_PARITY;
    myUART.Mode = UART_ENABLE_RX | UART_ENABLE_TX;
    myUART.FlowCtrl = UART_NONE_FLOW_CTRL;

    UART_Enable(UART0);
    UART_Init(UART0, &myUART);

	flash_add = DEMO_A_FLASH;
	while(1)
	{
		ret_val = UART_GetBufState(UART0, UART_TX);
		if(ret_val == DONE)
		{
		 	UART_SetTxData(UART0,'S');
		}

		for(i = 0;i < 512;)
		{
			ret_val = UART_GetBufState(UART0, UART_RX);
			if(ret_val == DONE)
			{
			 	rec_data[i] = UART_GetRxData(UART0);
				i++;
			}

		}		

		/* Whole page is not received yet */
		if(i < 512)
			continue;

		/* Check terminate command */
		if(0 == compare_str((const char *)rec_data, TERMINATION_STR, sizeof(TERMINATION_STR)-1))
			break;

		if (FC_SUCCESS == Write_Flash((uint32_t *)flash_add, (uint32_t *)rec_data, 512))
		{
			for(i = 0;i < 512;i++)
				rec_data[i] = 0;
		}
	 	else 
		{
	        return ERROR;
	    }

		flash_add = flash_add + 512;
	}

    return 0;
}




void SIO_Configuration(TSB_SC_TypeDef * SCx)
{
    if (SCx == UART0) {
        TSB_PE_CR_PE0C = 1U;
        TSB_PE_FR1_PE0F1 = 1U;
        TSB_PE_FR1_PE1F1 = 1U;
        TSB_PE_IE_PE1IE = 1U;
    } else if (SCx == UART1) {
        TSB_PE_CR_PE4C = 1U;
        TSB_PE_FR1_PE4F1 = 1U;
        TSB_PE_FR1_PE5F1 = 1U;
        TSB_PE_IE_PE5IE = 1U;
    } else if (SCx == UART2) {
        TSB_PF_CR_PF0C = 1U;
        TSB_PF_FR1_PF0F1 = 1U;
        TSB_PF_FR1_PF1F1 = 1U;
        TSB_PF_IE_PF1IE = 1U;
    } else {
        /* Do nothing */
    }
}
