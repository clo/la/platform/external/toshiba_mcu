/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    main.c
* @brief   the application functions of Read EEPROM demo for the TOSHIBA
*          'TMPM330' Device Series 
* @version V1.200
* @date    2010/6/23
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "main.h"

Result trx_result;

SBI_InitI2CTypeDef myI2C;

uint32_t gI2C_WCnt;
uint32_t gI2C_RCnt;
uint32_t gI2C_TxDataLen;
uint32_t gI2C_TxData[8] = { 0U };

uint32_t gI2C_RxDataLen;
uint32_t gI2C_RxData[8] = { 0U };

int main(void)
{
    SBI_I2CState i2c_state;
    SBI2_IO_Configuration();

/*------------------------- Initialize SBI channel 2 -------------------------*/
    myI2C.I2CSelfAddr = SELFADDR;
    myI2C.I2CDataLen = SBI_I2C_DATA_LEN_8;
    myI2C.I2CACKState = ENABLE;
    myI2C.I2CClkDiv = SBI_I2C_CLK_DIV_328;

    SBI_Enable(TSB_SBI2);
    SBI_SWReset(TSB_SBI2);
    SBI_InitI2C(TSB_SBI2, &myI2C);
    NVIC_Enable_Interrupt(INTSBI2_interrupt);

/*---------------------------- Write EEPROM 2 bytes --------------------------*/
    gI2C_TxData[0] = SUBADDR;
    gI2C_TxData[1] = DATA1;
    gI2C_TxData[2] = DATA2;
    gI2C_TxDataLen = 3U;
    gI2C_RxDataLen = 0U;
    gI2C_WCnt = 0U;
    do{
        i2c_state = SBI_GetI2CState(TSB_SBI2);
    } while (i2c_state.Bit.BusState);
    SBI_SetSendData(TSB_SBI2, SLAVEADDR | SBI_I2C_SEND);
    SBI_GenerateI2CStart(TSB_SBI2);
    while (gI2C_WCnt <= gI2C_TxDataLen);
    Delay();

/*---------------------------- Read EEPROM 2 bytes ---------------------------*/
    do{
        i2c_state = SBI_GetI2CState(TSB_SBI2);
    } while (i2c_state.Bit.BusState);
    gI2C_WCnt = 0U;
    gI2C_RCnt = 0U;
    gI2C_TxData[0] = SUBADDR;
    gI2C_TxDataLen = 1U;
    gI2C_RxDataLen = 2U;
    SBI_SetSendData(TSB_SBI2, SLAVEADDR | SBI_I2C_SEND);
    SBI_GenerateI2CStart(TSB_SBI2);

    while (gI2C_WCnt <= gI2C_TxDataLen);
    Delay();

    do{
        i2c_state = SBI_GetI2CState(TSB_SBI2);
    } while (i2c_state.Bit.BusState);

    SBI_SetSendData(TSB_SBI2, SLAVEADDR | SBI_I2C_RECEIVE);
    SBI_GenerateI2CStart(TSB_SBI2);

    while (gI2C_RCnt <= gI2C_RxDataLen);

/*------------------------- Check transmission result ------------------------*/
    if ((DATA1 == gI2C_RxData[0]) && (DATA2 == gI2C_RxData[1])) {
        trx_result = SUCCESS;
    } else {
        trx_result = ERROR;
    }

    while (1);
}

void Delay(void)
{
    volatile uint32_t i = 0U;
    for (; i <= 10000U; i++);
}

void SBI2_IO_Configuration(void)
{
    TSB_PG->DATA = 0U;
    TSB_PG->CR = 0x30U;
    TSB_PG->FR1 = 0x30U;
    TSB_PG->OD = 0x30U;
    TSB_PG->PUP = 0U;
    TSB_PG->IE = 0x30U;
}

#ifdef DEBUG
void assert_failed(char *file, int32_t line)
{
    while (1) {
        __NOP();
    }
}
#endif
