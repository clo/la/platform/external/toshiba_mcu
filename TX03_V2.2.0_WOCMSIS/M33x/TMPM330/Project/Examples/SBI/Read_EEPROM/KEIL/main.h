/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    main.h
* @brief   all the functions prototypes of Read EEPROM demo for the TOSHIBA
*          'TMPM330' Device Series 
* @version V1.200
* @date    2010/6/23
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
/* Includes ------------------------------------------------------------------*/
#include "tmpm330_sbi.h"
#include "tx03_common.h"

#define SELFADDR    0xE0U
#define SLAVEADDR   0xA0U
#define SUBADDR     0x0000U
#define DATA1       'S'
#define DATA2       'H'
#define SBI_I2C_SEND                       ((uint32_t)0x00000000)
#define SBI_I2C_RECEIVE                    ((uint32_t)0x00000001)


void Delay(void);
void SBI2_IO_Configuration(void);

#endif                          /* __MAIN_H */
