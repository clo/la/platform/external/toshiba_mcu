/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    main.c
* @brief   the application functions of CEC demo for the TOSHIBA
*          'TMPM330' Device Series 
* @version V1.200
* @date    2010/06/29
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "tmpm330_cec.h"
#include "uart_print_interface.h"

typedef enum {
    DATA_INIT = 0,
    DATA_IN_PROGRESS = 1,
    DATA_END = 2
} CEC_DataState;

typedef struct {
    CEC_LogicalAddr Initiator;
    CEC_LogicalAddr Destination;
    uint8_t Opcode;
    uint8_t CEC_Data[17];
    uint8_t current_num;
    uint8_t Max_num;
    CEC_DataState current_state;
} CEC_FrameTypeDef;

CEC_FrameTypeDef CEC_RxFrame;
CEC_FrameTypeDef CEC_RxFrameTmp;
CEC_FrameTypeDef CEC_TxFrame;

void CEC_Send_Frame(CEC_LogicalAddr Initiator, CEC_LogicalAddr Destination,
                    uint8_t Opcode, uint8_t OperandNum, uint8_t Operand[17]);

int main(void)
{
    uint32_t Index = 0U;
    uint32_t temp = 0U;
    SystemInit();

	UART_Print_Init();

    /*initial cec port */
    TSB_PK->CR = 0x01U;
    TSB_PK->FR1 = 0x01U;
    TSB_PK->IE = 0x01U;
    /*initial cec frame data buffer */
    CEC_RxFrame.Initiator = CEC_UNKNOWN;
    CEC_RxFrame.Destination = CEC_UNKNOWN;
    CEC_RxFrame.Opcode = 0xFFU;
    CEC_RxFrame.current_num = 0x0U;
    CEC_RxFrame.Max_num = 0x0U;
    CEC_RxFrame.current_state = DATA_INIT;
    for (Index = 0U; Index < 17U; Index++) {
        CEC_RxFrame.CEC_Data[Index] = 0xFFU;
    }
    CEC_RxFrameTmp = CEC_RxFrame;
    CEC_TxFrame = CEC_RxFrame;
    /*initial cec register */
    CEC_Enable();
    CEC_SWReset();
    while (CEC_GetRxState() == ENABLE);
    while (CEC_GetTxState() == BUSY);
    CEC_DefaultConfig();

    /*initial cec interupt */
    __disable_irq();
    TSB_CG->IMCGB = 0x00300000U;
    TSB_CG->IMCGD = 0x00000030U;
    TSB_CG->IMCGB = 0x00310000U;
    TSB_CG->IMCGD = 0x00000031U;
    TSB_CG->ICRCG = 0x06U;
    TSB_CG->ICRCG = 0x0cU;
    NVIC_Enable_Interrupt(INTCECRX_interrupt);
    NVIC_Enable_Interrupt(INTCECTX_interrupt);
    __enable_irq();

    /*set logical address TV */
    CEC_SetLogicalAddr(CEC_TV);
    /*Enable CEC reception */
    CEC_SetRxCtrl(ENABLE);

    while (1) {
        /*Recieve new frame or not */
        if (CEC_RxFrameTmp.current_state == DATA_END) {
            CEC_RxFrame = CEC_RxFrameTmp;
            CEC_RxFrameTmp.current_num = 0U;
            CEC_RxFrameTmp.current_state = DATA_INIT;
            temp = CEC_RxFrame.current_num;
#ifdef DEBUG
            if (temp > 0U) {
                uart_print_str("CEC Message: ");
                for (Index = 0U; Index < temp; Index++) {
					uart_print_byte(CEC_RxFrame.CEC_Data[Index]);
	                uart_print_str(" ");
                }
                uart_print_str("\n");
            }
#endif
            for (Index = 0U; Index < 17U; Index++) {
                CEC_RxFrameTmp.CEC_Data[Index] = 0xFFU;
            }
            /*CEC message is "Active Source" */
            if (CEC_RxFrame.CEC_Data[1] == 0x82U) {
                CEC_Send_Frame(CEC_TV, CEC_BROADCAST, 0x36U, 0U, CEC_RxFrame.CEC_Data);
            }
        }
    }
}

/**
  * @brief  Send A frame to other device.
  * @param  Initiator: Initiaor address ,Destination: Destination address, Opcode: operate code, OperandNum: Operand number,CEC_Data: CEC frame data
  * @retval None
  */
void CEC_Send_Frame(CEC_LogicalAddr Initiator, CEC_LogicalAddr Destination,
                    uint8_t Opcode, uint8_t OperandNum, uint8_t CEC_Data[17])
{
    uint32_t Index = 0U;
    if (CEC_TxFrame.current_state != DATA_IN_PROGRESS) {
        CEC_TxFrame.Initiator = Initiator;
        CEC_TxFrame.Destination = Destination;
        CEC_TxFrame.current_num = 0U;
        CEC_TxFrame.Opcode = Opcode;
        CEC_TxFrame.Max_num = 2U + OperandNum;
        CEC_TxFrame.current_state = DATA_IN_PROGRESS;
        CEC_TxFrame.CEC_Data[0] = (uint8_t) Initiator << 4U;
        CEC_TxFrame.CEC_Data[0] |= ((uint8_t) Destination);
        CEC_TxFrame.CEC_Data[1] = Opcode;

        for (Index = 2U; Index < 17U; Index++) {
            if (Index < CEC_TxFrame.Max_num) {
                CEC_TxFrame.CEC_Data[Index] = CEC_Data[Index];
            } else {
                CEC_TxFrame.CEC_Data[Index] = 0xFFU;
            }
        }
        if (CEC_BROADCAST == Destination) {
            CEC_SetTxBroadcast(ENABLE);
        } else {
            CEC_SetTxBroadcast(DISABLE);
        }
        /*Send first byte of frame */
        CEC_SetTxData(CEC_TxFrame.CEC_Data[0], CEC_NO_EOM);
        CEC_StartTx();
    }
}

#ifdef DEBUG
void assert_failed(char *file, int32_t line)
{
    while (1) {
        __NOP();
    }
}
#endif
