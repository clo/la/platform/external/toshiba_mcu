/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* @file    uart_print_interface.h
* @brief   Header file for the application functions to print data over UART0 
*          for the TOSHIBA 'TMPM330' Device Series 
* @version V1.000
* @date    2012/10/17
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/
#ifndef __UART_H
#define __UART_H

/* Includes ------------------------------------------------------------------*/
#include "tmpm330_uart.h"

/* Private typedef -----------------------------------------------------------*/
typedef enum { NO = 0, YES = 1 } TrxState;

/* Private define ------------------------------------------------------------*/
#define BUFFER_SIZE 128
#define SET     0x01                    /* flag is set */
#define CLEAR   0x00                    /* flag is cleared */

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* external variables --------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
extern void UART_Print_Init(void);
extern uint8_t send_char(uint8_t ch);

extern void uart_print_str(int8_t *str);
extern void uart_print_byte(uint8_t byte_val);
extern void uart_print_short(uint16_t short_val);
extern void uart_print_word(uint32_t word_val);
extern int uart_get_char(void);

#endif
/******************************** END OF FILE *********************************/
