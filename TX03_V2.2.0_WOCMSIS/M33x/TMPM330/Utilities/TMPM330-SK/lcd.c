/*******************************************************************************
* Copyright(C) 2012 Toshiba America Electronic Components, Inc.(TAEC)  
* All rights reserved
*
* sample for TMPM330FxFG
*
* File Name          : led.c
* Version            : 
* Date               : 
* Description        : 
*
* Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. TAEC ASSUMES NO LIABILITY FOR CUSTOMERS' 
*   PRODUCT DESIGN OR APPLICATIONS.
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "lcd.h"
#include <string.h>

/*******************************************************************************
* Function Name  : E_Pulse
* Description    : Generation a pulse enable the LCD.
* Input          : None.
* Return         : None.
*******************************************************************************/
void E_Pulse(void)
{
    // set E high
    TSB_PH_DATA_PH1 = 1;
    Delay(10);
    // set E low
    TSB_PH_DATA_PH1 = 0;
}

/*******************************************************************************
* Function Name  : Set_Data
* Description    : transfer data to LCD
* Input          : data --- The data will transfer to LCD
* Return         : None.
*******************************************************************************/
void Set_Data(unsigned char data)
{
    uint8_t cTemp;
    cTemp = TSB_PF->DATA & 0x0F;
    data = ((uint8_t) (data << 4)) | cTemp;
    TSB_PF->DATA = data;
}

/*******************************************************************************
* Function Name  : Send_To_LCD
* Description    : send command or data to LCD
* Input          : data --- command or data
*                  type --- LCD_COMMAND or LCD_CHARACTER
* Return         : None.
*******************************************************************************/
void Send_To_LCD(unsigned char data, unsigned char type)
{
    // enable write
    TSB_PH_DATA_PH2 = 0;
    Delay(2);
    // set high nibble of command
    Set_Data((data >> 4));
    // set RS port to set command mode or data
    if (type == LCD_COMMAND)
        TSB_PH_DATA_PH3 = 0;
    else
        TSB_PH_DATA_PH3 = 1;
    // pulse to set D4-D7 bits
    E_Pulse();
    // set low nibble of command
    Set_Data((data & 0x0F));
    // set RS port to set command mode or data
    if (type == LCD_COMMAND)
        TSB_PH_DATA_PH3 = 0;
    else
        TSB_PH_DATA_PH3 = 1;
    // pulse to set D4-D7 bits
    E_Pulse();
}

/*******************************************************************************
* Function Name  : Send_LCD_Text
* Description    : Send the string to LCD
* Input          : str --- pointer of the string
* Return         : None.
*******************************************************************************/
void Send_LCD_Text(char *str)
{
    unsigned char i;
    /* loop over string and send each character */
    for (i = 0; i < strlen(str); i++) {
        Send_To_LCD(str[i], LCD_CHARACTER);
    }
    for (; i < 16; i++)
        Send_To_LCD(' ', LCD_CHARACTER);
}

void LCD_IO(void)
{
    TSB_PF->CR |= 0x00F4;
    TSB_PF->PUP |= 0x00F4;
    TSB_PH->CR |= 0x0E;
    TSB_PH->PUP |= 0x0E;
}

/*******************************************************************************
* Function Name  : LCD_Configuration
* Description    : Configures LCD
* Input          : None.
* Return         : None.
*******************************************************************************/
void LCD_Configuration(void)
{
    LCD_IO();

    /* LCD initialization step by step */
    // clear "command" bits
    TSB_PH_DATA_PH3 = 0;
    TSB_PH_DATA_PH2 = 0;
    TSB_PH_DATA_PH1 = 0;
    Delay(10);

    // set D4 and D5 high
    TSB_PF_DATA_PF4 = 1;
    TSB_PF_DATA_PF5 = 1;
    E_Pulse();
    Delay(10);
    // set D4 and D5 high
    TSB_PF_DATA_PF4 = 1;
    TSB_PF_DATA_PF5 = 1;
    E_Pulse();
    Delay(10);
    // set D4 and D5 high
    TSB_PF_DATA_PF4 = 1;
    TSB_PF_DATA_PF5 = 1;
    E_Pulse();
    Delay(10);
    // set D4 to low and D5 to high
    TSB_PF_DATA_PF4 = 0;
    TSB_PF_DATA_PF5 = 1;
    E_Pulse();
    // enable display
    Send_To_LCD(DISP_ON, LCD_COMMAND);
    // clear display
    Send_To_LCD(CLR_DISP, LCD_COMMAND);
}

/*******************************************************************************
* Function Name  : LCD_Light
* Description    : Turn on or off the back light of LCD
* Input          : BLStatus --- ON or OFF
* Return         : None.
*******************************************************************************/
void LCD_Light(unsigned char BLStatus)
{
    if (BLStatus == ON)
        TSB_PF_DATA_PF2 = 1;
    else
        TSB_PF_DATA_PF2 = 0;
}

void Delay(int time)
{
    volatile int i = 0;

    for (i = 0; i < (time * 800); ++i);
}
